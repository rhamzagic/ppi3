Router.configure({
	waitOn: function() {
		return [Meteor.subscribe('campi'), Meteor.subscribe('rooms'), Meteor.subscribe('categories'), Meteor.subscribe('subcategories'), Meteor.subscribe('reports')];
	}
});
Router.map(function() {
	this.route('adminCategoriesList', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/categories'
	});
});

Router.map(function() {
	this.route('adminCategoryEdit', {
		layoutTemplate: 'adminLayout',
		path: 'admin/categories/:_id',
		loadingTemplate: 'loading',
		data: function(){
			return Categories.findOne(this.params._id);
		}
	});
});

Router.map(function() {
	this.route('adminCategoryNew', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/category/new'
	});
});


Router.map(function() {
	this.route('adminSubcategoriesList', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/subcategories'
	});
});

Router.map(function() {
	this.route('adminSubcategoryEdit', {
		layoutTemplate: 'adminLayout',
		path: 'admin/subcategories/:_id',
		loadingTemplate: 'loading',
		data: function(){
			return Subcategories.findOne(this.params._id);
		}
	});
});

Router.map(function() {
	this.route('adminSubcategoryNew', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/subcategory/new'
	});
});


Router.map(function() {
	this.route('adminCampiList', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/campi'
	});
});

Router.map(function() {
	this.route('adminCampusEdit', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/campi/:_id',
		data: function(){
			return Campi.findOne(this.params._id);
		}
	});
});

Router.map(function() {
	this.route('adminCampusNew', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/campus/new'
	});
});


Router.map(function() {
	this.route('adminRoomsList', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/rooms'
	});
});

Router.map(function() {
	this.route('adminRoomEdit', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/rooms/:_id',
		data: function(){
			return Rooms.findOne(this.params._id);
		}
	});
});

Router.map(function() {
	this.route('adminRoomNew', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/room/new'
	});
});

Router.map(function(){
	this.route('adminReportsList', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin'
	});
});

Router.map(function(){
	this.route('adminReportEdit', {
		layoutTemplate: 'adminLayout',
		loadingTemplate: 'loading',
		path: 'admin/reports/:_id',
		data: function(){
			return Reports.findOne(this.params._id);
		}
	});
});

var requireLogin = function(pause) {
	if (! Meteor.user()) {
	    if (Meteor.loggingIn())
	    	this.render(this.loadingTemplate);
	    else
	    	this.render('accessDenied');
	    pause();
	}
}

Router.onBeforeAction('loading');
Router.onBeforeAction(
	requireLogin, {only: ['adminCategoriesList','adminCategoryEdit','adminCategoryNew','adminSubcategoriesList', 'adminSubcategoryEdit', 'adminSubcategoryNew', 'adminCampiList', 'adminCampusEdit', 'adminCampusNew', 'adminRoomsList', 'adminRoomEdit', 'adminRoomNew', 'adminReportsList', 'adminReportEdit']});
