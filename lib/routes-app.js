

Router.map(function() {
	this.route('reportsList', {
		path: '/',
		layoutTemplate: 'appLayout',
		loadingTemplate: 'appLoading'
	});
});

Router.map(function() {
	this.route('campusReportsList', {
		path: 'reports/:_id',
		layoutTemplate: 'appLayout',
		loadingTemplate: 'appLoading',
		data: function() {
			var campus = Campi.findOne(this.params._id);
			var reports = Reports.find({campusId:this.params._id, status: {'$not': 'Resolvido'}}).map(function(doc){
				console.log('doc', doc);
				return reportGetData(doc);
			});
			return {
				campusName: campus.name,
				reports: reports
			};
		}
	});
});


Router.map(function() {
	this.route('reportView', {
		path: 'report/:_id',
		layoutTemplate: 'appLayout',
		loadingTemplate: 'appLoading',
		data: function(){
			var report = Reports.findOne(this.params._id);
			if (report) {
				return reportGetData(report);
			}
		}
	});
});


Router.map(function() {
	this.route('reportNew', {
		layoutTemplate: 'appLayout',
		loadingTemplate: 'appLoading',
		path: 'new'
	});
});
