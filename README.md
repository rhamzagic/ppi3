# Boletim de chamados técnicos

Projeto criado para a disciplina de PPI (Projeto Profissional Integrado) da Faculdade Sumaré.

Seu objetivo de fornecer um meio onde qualquer pessoa possa reportar problemas ou defeitos em qualquer equipamento, ou infraestrutura,  de qualquer um dos campi da Faculdade Sumaré.


## Módulos

### Cliente

Qualquer pessoa pode abrir um novo chamado, indicando os detalhes pertinentes ao problema. Pode ver também todos os chamados que estão em manutenção.

Ao criar um novo chamado, o usuário indica o campus e a sala em que o equipamento está localizado, se for relevante. Pode fornecer seu endereço de e-mail para ser notificado quando o chamado for resolvido.
O chamado pode possuir três estados, ou "status":

* Em aberto: quando foi reportado pelo usuário e ainda não foi verificado pela equipe técnica da faculdade;
* Em manutenção: após verificado pela equipe técnica e está em manutenção;
* Resolvido: após o problema ter sido resolvido, ou o equipamento trocado;

A lista de chamados indica apenas itens com os estados "Em aberto" e "Em manutenção".

Clique [aqui](http://sumare_ppi.meteor.com "Cliente") para acessar o cliente para ver e reportar itens.


### Área administrativa

Através da área administrativa os funcionários podem gerenciar:

* Campi;
* Salas;
* Categorias;
* Subcategorias;
* Chamados;

É possível cadastrar novas categorias (e subcategorias), afim de abranger um maior número de itens possíveis de serem reportados.

Clique [aqui](http://sumare_ppi.meteor.com/admin "Área administrativa") para acessar a área administrativa.


## Recursos

### Código fonte

Foi adotado o sistema de controle de versão (SCV) [GIT](http://pt.wikipedia.org/wiki/Git "GIT (Wikipedia)"). Todo o código está  disponível publicamente no repositório:
<https://bitbucket.org/rhamzagic/ppi3>


### Tecnologia

* Linguagem: Javascript (cliente e servidor);
* Framework: [Meteor](http://www.meteor.com "Meteor");
* Design responsivo: [Bootstrap](http://getbootstap.com) (ajustável à largura do device);
* Banco de dados: [MongoDB](http://www.mongodb.org "MongoDB") (Não relacional);
* Envio de e-mails: [Mailgun](https://mailgun.com "Mailgun")[^1];

[^1]: Com exceção do serviço "Mailgun", todos os softwares utilizados no projeto, são livre :)


### Compatibilidade

O projeto é compatível com navegadores de smartphones, tablets e computadores desktop:

* Android (baseado em Webkit)
* iOS (Safari)
* Chrome
* Opera
* Firefox 7 e superiores
* IE8 e superiores
* OS X Safari 4 e superiores

## Agradecimentos
Aos professores: **Andre Luiz Cirino da Silva** e **Rodinei Cesar Pontelli**, que estiverem sempre disponíveis para orientações e dúvidas.