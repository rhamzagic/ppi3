Meteor.publish('rooms', function() {
    return Rooms.find();
});
Meteor.publish('campi', function() {
    return Campi.find();
});
Meteor.publish('categories', function() {
    return Categories.find();
});
Meteor.publish('subcategories', function() {
    return Subcategories.find();
});
Meteor.publish('reports', function() {
    return Reports.find();
});
