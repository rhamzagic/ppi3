if (Categories.find().count() === 0) {
    Categories.insert({"_id": "cat--equipamentos", "name" : "Equipamentos"});
    Categories.insert({"_id": "cat---------infra", "name" : "Infraestrutura"});
    //Categories.insert({"_id": "cat--------moodle", "name" : "Moodle"});
}

if (Subcategories.find().count() === 0) {
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Mouse"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Monitor"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Teclado"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Gabinete (CPU)"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Notebook"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Projetor Multimídia"});
    Subcategories.insert({"categoryId": "cat--equipamentos", "name":"Caixas de som"});

    //Subcategories.insert({"categoryId": "cat--equipamentos", "name":""});
    Subcategories.insert({"categoryId": "cat---------infra", "name":"Acesso à rede"});
    Subcategories.insert({"categoryId": "cat---------infra", "name":"Acesso à internet"});
    Subcategories.insert({"categoryId": "cat---------infra", "name":"Catacra de acesso"});

    //Subcategories.insert({"categoryId": "cat--------moodle", "name":"Indisponibilidade"});
    //Subcategories.insert({"categoryId": "cat--------moodle", "name":"Acesso negado"});
}

if (Campi.find().count() === 0) {
    Campi.insert({"_id": "campus------belem", "name" : "Belém"});
    Campi.insert({"_id": "campus----santana", "name" : "Santana"});
    Campi.insert({"_id": "campus-bom-retiro", "name" : "Bom Retiro"});
    Campi.insert({"_id": "campus-----imirim", "name" : "Imirim"});
    Campi.insert({"_id": "campus--sto-amaro", "name" : "Santo Amaro"});
    Campi.insert({"_id": "campus-----sumare", "name" : "Sumaré"});
    Campi.insert({"_id": "campus--tatuape-1", "name" : "Tatuapé I"});
    Campi.insert({"_id": "campus--tatuape-2", "name" : "Tatuapé II"});
}


if (Rooms.find().count() === 0) {
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus------belem", "_id" : "f7376qGQHxDDxPYHY" });
    Rooms.insert({ "name" : "10", "campusId" : "campus------belem" });
    Rooms.insert({ "name" : "11", "campusId" : "campus------belem" });
    Rooms.insert({ "name" : "12", "campusId" : "campus------belem" });

    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus-bom-retiro", "_id" : "dvjuBrysg9xEmcAuB" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus-----imirim", "_id" : "cuMLMDKQNhbTNpjZc" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus----santana", "_id" : "tHAoqWi2ptCMwJLG5" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus--sto-amaro", "_id" : "qAWWSiajH7GcoatvK" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus-----sumare", "_id" : "6takC8yNWZ8Fdc2q3" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus--tatuape-1", "_id" : "9B3dEGppsuRwZMvbk" });
    Rooms.insert({ "name" : "Biblioteca", "campusId" : "campus--tatuape-2", "_id" : "i52zx3DudFXsAmsR5" });

    Rooms.insert({ "name" : "Laboratório 10", "campusId" : "campus-----sumare" });
    Rooms.insert({ "name" : "11", "campusId" : "campus-----sumare"});
    Rooms.insert({ "name" : "12", "campusId" : "campus-----sumare"});
    Rooms.insert({ "name" : "13", "campusId" : "campus-----sumare"});
    Rooms.insert({ "name" : "14", "campusId" : "campus-----sumare"});
}
