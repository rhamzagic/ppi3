Template.adminRoomEdit.helpers({
	campi: function(){
		var campusId = this.campusId;
		var arrCampi;
		Deps.autorun(function() {
			arrCampi = Campi.find({}, {sort:{name:1}}).map(function(doc){
				doc.selected = (doc._id == campusId);
				return doc;
			});
		});
		return arrCampi;
	}
});


Template.adminRoomEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var data = {
			name: $.trim( $(e.target).find('[name=name]').val() ),
			campusId: $(e.target).find('[name=campus]').val()
		};

		Meteor.call('roomUpdate', this._id, data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminRoomsList');
			}
		});
	},

	'click .deletePrompt': function(e) {
		e.preventDefault();
		$('.modal').modal('show');
	},

	'click .delete': function(e) {
		Rooms.remove(this._id);
		$('.modal').on('hidden.bs.modal', function(e){
			$(this).off('hidden.bs.modal');
			Router.go('adminRoomsList');
		});
	}
});
