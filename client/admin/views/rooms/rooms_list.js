Template.adminRoomsList.helpers({
	rooms: function(){
		var arr = Rooms.find().map(function(doc){
			var campus = Campi.findOne(doc.campusId);

			var campusName = (campus) ? campus.name : 'Sem campus';
			return {
				_id: doc._id,
				name: doc.name,
				campusName: campusName
			}
		});
		return _.sortBy(arr, function(item){
			return item.campusName + item.name;
		});

	}
});
