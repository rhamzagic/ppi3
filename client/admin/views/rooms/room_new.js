Template.adminRoomNew.helpers({
	campi: function(){
		return Campi.find({}, {sort:{name:1}});
	}
});


Template.adminRoomNew.events({
	'submit form': function(e) {
		e.preventDefault();

		var objData = {
			name: $.trim( $(e.target).find('[name=name]').val() ),
			campusId: $(e.target).find('[name=campus]').val()
		};

		Meteor.call('room', objData, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminRoomsList');
			}
		});
	}
});
