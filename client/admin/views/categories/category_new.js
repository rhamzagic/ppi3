Template.adminCategoryNew.events({
	'submit form': function(e) {
		e.preventDefault();

		var objData = {
			name: $.trim( $(e.target).find('[name=name]').val() )
		};

		Meteor.call('category', objData, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminCategoriesList');
			}
		});
	}
});
