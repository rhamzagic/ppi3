Template.adminCategoryEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var data = {
			name: $.trim( $(e.target).find('[name=name]').val() )
		};

		Meteor.call('categoryUpdate', this._id, data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminCategoriesList');
			}
		});
	},

	'click .deletePrompt': function(e) {
		e.preventDefault();
		$('.modal').modal('show');
	},

	'click .delete': function(e) {
		Categories.remove(this._id);
		$('.modal').modal('hide');
		$('.modal').on('hidden.bs.modal', function (e) {
			$('.modal').off('hidden.bs.modal');
			Router.go('adminCategoriesList');
		});
	}
});
