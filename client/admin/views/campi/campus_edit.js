Template.adminCampusEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var data = {
			name: $.trim( $(e.target).find('[name=name]').val() )
		};

		Meteor.call('campusUpdate', this._id, data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminCampiList');
			}
		});
	},

	'click .deletePrompt': function(e) {
		e.preventDefault();
		$('.modal').modal('show');
	},

	'click .delete': function(e) {
		Campi.remove(this._id);
		$('.modal').on('hidden.bs.modal', function(e){
			$(this).off('hidden.bs.modal');
			Router.go('adminCampiList');
		});
	}
});
