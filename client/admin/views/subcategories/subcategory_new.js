Template.adminSubcategoryNew.helpers({
	categories: function(){
		return Categories.find({}, {sort:{name:1}});
	}
});

Template.adminSubcategoryNew.events({
	'submit form': function(e) {
		e.preventDefault();

		var data = {
			name: $.trim( $(e.target).find('[name=name]').val() ),
			categoryId: $(e.target).find('[name=category]').val()
		};


		Meteor.call('subcategory', data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminSubcategoriesList');
			}
		});

	}
});
