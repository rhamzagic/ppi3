Template.adminSubcategoriesList.helpers({
	subcategories: function(){
		var arr = Subcategories.find().map(function(doc){
			var category = Categories.findOne(doc.categoryId);
			var categoryName = (category) ? category.name : 'Sem categoria';
			return {
				_id: doc._id,
				name: doc.name,
				categoryName: categoryName
			}
		});
		return _.sortBy(arr, function(item){
			return item.categoryName + item.name;
		});
	}
});
