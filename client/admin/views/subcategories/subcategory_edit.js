Template.adminSubcategoryEdit.helpers({
	categories: function(){
		var categoryId = this.categoryId;
		var arrCategories;
		Deps.autorun(function(){
			arrCategories = Categories.find({}, {sort:{name:1}}).map(function(doc){
				doc.selected = doc._id == categoryId;
				return doc;
			});
		});
		return arrCategories;
	}
});
Template.adminSubcategoryEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var data = {
			name: $.trim( $(e.target).find('[name=name]').val() ),
			categoryId: $(e.target).find('[name=category]').val()
		};

		Meteor.call('subcategoryUpdate', this._id, data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});
			} else {
				Router.go('adminSubcategoriesList');
			}
		});
	},

	'click .deletePrompt': function(e) {
		e.preventDefault();
		$('.modal').modal('show');
	},

	'click .delete': function(e) {
		Subcategories.remove(this._id);
		$('.modal').modal('hide');
		$('.modal').on('hidden.bs.modal', function (e) {
			$('.modal').off('hidden.bs.modal');
			Router.go('adminSubcategoriesList');
		});
	}
});
