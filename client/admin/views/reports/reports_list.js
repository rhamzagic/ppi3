Template.adminReportsList.helpers({
    reports: function(){
        var reports = Reports.find().map(function(doc){
            return reportGetData(doc);
        });
        return reports;
    }
});
