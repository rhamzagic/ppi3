Template.adminReportEdit.created = function() {
	Session.set('adminCampusId', undefined);
	Session.set('adminCategoryId', undefined);
}

Template.adminReportEdit.helpers({
	campi: function(){
		var id = Session.get('adminCampusId') || this.campusId;
		var arr;
		Deps.autorun(function(){
			arr = Campi.find({}, {sort:{name:1}}).map(function(doc){
				doc.selected = doc._id == id;
				return doc;
			});
		});
		return arr;
	},
	rooms: function(){
		var id = this.roomId;
		var campusId = Session.get('adminCampusId') || this.campusId;
		var arr;
		Deps.autorun(function(){
			arr = Rooms.find({campusId:campusId}, {sort:{name:1}}).map(function(doc){
				doc.selected = doc._id == id;
				return doc;
			});
		});
		return arr;
	},
	categories: function(){
		var id = Session.get('adminCategoryId') || this.categoryId;
		var arr;
		Deps.autorun(function(){
			arr = Categories.find({}, {sort:{name:1}}).map(function(doc){
				doc.selected = doc._id == id;
				return doc;
			});
		});
		return arr;
	},
	subcategories: function(){
		var id = this.subcategoryId;
		var categoryId = Session.get('adminCategoryId') || this.categoryId;
		var arr;
		Deps.autorun(function(){
			arr = Subcategories.find({categoryId:categoryId}, {sort:{name:1}}).map(function(doc){
				doc.selected = doc._id == id;
				return doc;
			});
		});
		return arr;
	},
	statuses: function(){
		var status = this.status;
		var arr = [
			{name: 'Em aberto'},
			{name: 'Em manutenção'},
			{name: 'Resolvido'}
		];
		arr = _.map(arr, function(item){
			item.selected = item.name == status;
			return item;
		});
		return arr;
	}
});


Template.adminReportEdit.events({
	'change form select[name=campus]': function(e) {
		var id = $(e.currentTarget).val();
		Session.set('adminCampusId', id);
	},

	'change form select[name=category]': function(e) {
		var id = $(e.currentTarget).val();
		Session.set('adminCategoryId', id);
	},

	'submit form': function(e) {
		e.preventDefault();

		var data = {
			campusId: $(e.target).find('[name=campus]').val(),
			status: $(e.target).find('[name=status]').val(),
			roomId: $(e.target).find('[name=room]').val(),
			categoryId: $(e.target).find('[name=category]').val(),
			subcategoryId: $(e.target).find('[name=subcategory]').val(),
			title: $(e.target).find('[name=title]').val(),
			description: $(e.target).find('[name=description]').val(),
			itemCode: $(e.target).find('[name=itemCode]').val(),
			email: $.trim( $(e.target).find('[name=email]').val() )
		};

		Meteor.call('reportUpdate', this._id, data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});

			} else {
				Router.go('adminReportsList');
			}
		});
	},

	'click .deletePrompt': function(e) {
		e.preventDefault();
		$('.modal').modal('show');
	},

	'click .delete': function(e) {
		Reports.remove(this._id);
		$('.modal').modal('hide');
		$('.modal').on('hidden.bs.modal', function (e) {
			$('.modal').off('hidden.bs.modal');
			Router.go('adminReportsList');
		});

	}
});
