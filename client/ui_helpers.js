UI.registerHelper("dateFormat", function(date) {
    date = EJSON.parse(date);
    moment.lang('pt-br');
    return  moment(date).fromNow(); //moment(date).format('DD/MM/YYYY')
});


UI.registerHelper("statusGetCssClass", function(status, prefix){
    var cssClass;
    switch(status) {
        case 'Reportado':
            cssClass = 'danger';
            break;
        case 'Em aberto':
            cssClass = 'warning';
            break;
        case 'Em manutenção':
            cssClass = 'success';
            break;
        case 'Resolvido':
            cssClass = 'info';
            break;
    };
    if (prefix)
        cssClass = prefix + cssClass;
    return cssClass;
});


UI.registerHelper("statusGetIcon", function(status, prefix){
    var icon;
    switch(status) {
        case 'Em aberto':
            icon = 'flag';
            break;
        case 'Em manutenção':
            icon = 'wrench';
            break;
        case 'Resolvido':
            icon = 'ok';
            break;
    };
    if (prefix)
        icon = prefix + icon;
    return icon;
})
