Template.reportsList.helpers({
    reports: function(){
        var reports = Reports.find({status: {'$ne': 'Resolvido'}}).map(function(doc){
            return reportGetData(doc);
        });

        console.log('reports', reports);
        return reports;
    }
});
