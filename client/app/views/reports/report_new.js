Template.reportNew.helpers({
	campi: function(){
		return Campi.find({}, {sort:{name:1}});
	},
	rooms: function(){
		if (Session.get('campusId')) {
			var campus = Campi.findOne(Session.get('campusId'));
		} else {
			var campus = Campi.findOne({}, {sort:{name:1}});
		}
		if (campus) {
			console.log('campus', campus);
			return Rooms.find({campusId:campus._id}, {sort:{name:1}});
		}
	},
	categories: function(){
		return Categories.find({}, {sort:{name:1}});
	},
	subcategories: function(){
		if (Session.get('categoryId')) {
			var category = Categories.findOne(Session.get('categoryId'));
		} else {
			var category = Categories.findOne({}, {sort:{name:1}});
		}
		if (category) {
			return Subcategories.find({categoryId:category._id}, {sort:{name:1}});
		}
	}
});

Template.reportNew.events({
	'change form select[name=campus]': function(e) {
		var id = $(e.currentTarget).val();
		Session.set('campusId', id);
	},

	'change form select[name=category]': function(e) {
		var id = $(e.currentTarget).val();
		Session.set('categoryId', id);
	},

	'submit form': function(e) {
		e.preventDefault();

		var data = {
			campusId: $(e.target).find('[name=campus]').val(),
			roomId: $(e.target).find('[name=room]').val(),
			categoryId: $(e.target).find('[name=category]').val(),
			subcategoryId: $(e.target).find('[name=subcategory]').val(),
			title: $(e.target).find('[name=title]').val(),
			description: $(e.target).find('[name=description]').val(),
			itemCode: $(e.target).find('[name=itemCode]').val(),
			email: $.trim( $(e.target).find('[name=email]').val() )
		};

		Meteor.call('report', data, function(error, id) {
			if (error) {
				$.pnotify({
					title: 'Erro',
					text: error.reason,
					type: 'error',
					icon: false,
					mouse_reset: false,
					animate_speed: 'fast',
					buttons: {
						sticker: false
					}
				});

			} else {
				Router.go('reportsList');
			}
		});
	}
});
