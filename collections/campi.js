Campi = new Meteor.Collection('campi');

Meteor.methods({
    campus: function(data) {

        if (!data.name)
            throw new Meteor.Error(422, 'Por favor, indique o nome do campus');

        if (Campi.findOne({name: data.name}))
            throw new Meteor.Error(302, 'Já existe um campus com este nome');

        return Campi.insert({name:data.name});
    },
    campusUpdate: function(id, data) {
        if (!data.name)
            throw new Meteor.Error(422, 'Por favor, indique o nome do campus');

        if (Campi.findOne({name: data.name}))
            throw new Meteor.Error(302, 'Já existe um campus com este nome');

        Campi.update(id, {$set: data});
    }
});
