Reports = new Meteor.Collection('reports');

Meteor.methods({
    report: function(data) {
        if (!data.campusId)
            throw new Meteor.Error(422, 'Por favor, indique o campus');

        if (!data.roomId)
            throw new Meteor.Error(422, 'Por favor, indique a sala');

        if (!data.categoryId)
            throw new Meteor.Error(422, 'Por favor, indique a categoria do problema');

        if (!data.subcategoryId)
            throw new Meteor.Error(422, 'Por favor, indique o tipo de item com problema');

        if (!data.title)
            throw new Meteor.Error(422, 'Por favor, digite o título do problema');

        data = _.extend(_.pick(data, 'campusId', 'roomId', 'categoryId', 'subcategoryId', 'title', 'description', 'itemCode','email'), {});
        data.createdAt = data.updatedAt = EJSON.stringify(new Date());
        /* status:
        - Em aberto -> Em manutenção -> Resolvido
        */
        data.status = 'Em aberto';

        return Reports.insert(data);
    },
    reportUpdate: function(id, data) {
        var report = Reports.findOne(id);
        var currentStatus = report.status;

        data = _.extend(_.pick(data, 'campusId', 'roomId', 'categoryId', 'subcategoryId', 'title', 'description', 'itemCode','email', 'status'), {});
        data.updatedAt = EJSON.stringify(new Date());
        var result = Reports.update(id, {$set: data});

        if (data.status=='Resolvido' && data.email) {
            if (currentStatus != data.status) {
                Email.send({
                    to: data.email,
                    from: 'naoresponda@sumare.edu.br',
                    subject: '[Manutenção Faculdade Sumaré] Chamado resolvido',
                    text: 'O chamado que você abriu, com o título "' + data.title + '", foi resolvido.\nObrigado pela colaboração!\n\nEquipe de manutenção Faculdade Sumaré'
                });
            }

        }

    }

});


reportGetData = function (report) {
    var campus = Campi.findOne(report.campusId);
    var room = Rooms.findOne(report.roomId);
    var category = Categories.findOne(report.categoryId);
    var subcategory = Subcategories.findOne(report.subcategoryId);

    report.campusName = (campus) ? campus.name : 'Campus não informado';
    report.roomName = (room) ? room.name : 'Sala não informada';
    report.categoryName = (category) ? category.name : 'Categoria não informado';
    report.subcategoryName = (subcategory) ? subcategory.name : 'Tipo do item informado';

    return report;
}
