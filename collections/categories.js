Categories = new Meteor.Collection('categories');

Meteor.methods({
    category: function(data) {
        if (!data.name)
            throw new Meteor.Error(422, 'Por favor, indique o nome da categoria');

        if (Categories.findOne({name: data.name}))
            throw new Meteor.Error(302, 'Já existe uma categoria com este nome');

        return Categories.insert({name:data.name});
    },
    categoryUpdate: function(id, data) {
        if (!data.name)
            throw new Meteor.Error(422, 'Por favor, indique o nome da categoria');

        if (Categories.findOne({name: data.name}))
            throw new Meteor.Error(302, 'Já existe uma categoria com este nome');

        Categories.update(id, {$set: data});
    }
});
