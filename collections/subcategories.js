Subcategories = new Meteor.Collection('subcategories');

Meteor.methods({
	subcategory: function(data) {
		if (!data.categoryId)
			throw new Meteor.Error(422, 'Por favor, indique a categoria');

		if (!Categories.findOne(data.categoryId))
			throw new Meteor.Error(422, 'Categoria inválida');

		if (!data.name)
			throw new Meteor.Error(422, 'Por favor, indique o nome da subcategoria');

		newSubcategory = _.extend(_.pick(data, 'name', 'categoryId'), {});
		if (Subcategories.findOne(newSubcategory))
			throw new Meteor.Error(302, 'Já existe uma subcategoria com este nome');

		return Subcategories.insert(newSubcategory);
	},
	subcategoryUpdate: function(id, data) {
		if (!data.categoryId)
				throw new Meteor.Error(422, 'Por favor, indique a categoria');

			if (!Categories.findOne(data.categoryId))
				throw new Meteor.Error(422, 'Categoria inválida');

			if (!data.name)
				throw new Meteor.Error(422, 'Por favor, indique o nome da subcategoria');

		Subcategories.update(id, {$set: data});
	}
});
