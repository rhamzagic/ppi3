Rooms = new Meteor.Collection('rooms');

Meteor.methods({
    room: function(data) {

        if (!data.name)
            throw new Meteor.Error(422, 'Por favor, indique o nome da sala');

        if (Rooms.findOne({name: data.name, campusId:data.campusId}))
            throw new Meteor.Error(302, 'Já existe uma sala com este nome');

        if (!Campi.findOne({_id: data.campusId}))
            throw new Meteor.Error(422, 'Indique o campus da sala');

        data = _.extend(_.pick(data, 'name', 'campusId'), {});
        return Rooms.insert(data);
    },
    roomUpdate: function(id, data) {
        if (!data.name)
                throw new Meteor.Error(422, 'Por favor, indique o nome da sala');

        if (Rooms.findOne({_id: {$ne: id}, name: data.name, campusId:data.campusId}))
            throw new Meteor.Error(302, 'Já existe uma sala com este nome');


        if (!Campi.findOne({_id: data.campusId}))
            throw new Meteor.Error(422, 'Indique o campus da sala');

        data = _.extend(_.pick(data, 'name', 'campusId'), {});
        Rooms.update(id, {$set: data});
    }
});
